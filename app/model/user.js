const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
	// _id: {type: ObjectId, unique:true},
	username: {type: String, unique:true, required:true},
	firstname: {type: String, required:true},
	lastname: {type: String, required:true},
},{
	timestamps:true
});

module.exports = mongoose.model('User', User);
 