const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Voucher = new Schema({
	// _id: {type: ObjectId, unique:true},
	code: {type: String, unique:true, required:true},
	discount: {type: Number, required:true},
	note: {type: String, required:false},
	
},{
	timestamps:true
});

module.exports = mongoose.model('Voucher', Voucher);
  