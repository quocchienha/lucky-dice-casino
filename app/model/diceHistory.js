const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const DiceHistory = new Schema({
	// _id: {type: Schema.Types.ObjectId, unique:true},
	user: {type: mongoose.Types.ObjectId, ref:"User", required: true},
	dice:	{type: Number, require:true, default: () => Math.floor(Math.random() * 6) + 1},
},{
	timestamps: true
});

module.exports = mongoose.model('DiceHistory', DiceHistory);
 