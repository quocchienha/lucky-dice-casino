//khai báo thư viện express
const express = require('express');
const prizeHistoryMiddleware = require('../middlewares/prizeHistoryMiddleware');
const prizeHistoryController = require('../controllers/prizeHistoryController');
//tạo router
const prizeHistoryRouter = express.Router();

//sủ dụng middle ware
prizeHistoryRouter.use(prizeHistoryMiddleware);

//get all PrizeHistories
prizeHistoryRouter.get('/prize-histories', prizeHistoryController.getAllPrizeHistory);

//get a PrizeHistory
prizeHistoryRouter.get('/prize-histories/:historyId', prizeHistoryController.getPrizeHistoryById);

//create a PrizeHistory
prizeHistoryRouter.post('/prize-histories', prizeHistoryController.createPrizeHistory);

//update a PrizeHistory
prizeHistoryRouter.put('/prize-histories/:historyId', prizeHistoryController.updatePrizeHistoryById);

//delete a PrizeHistory

prizeHistoryRouter.delete('/prize-histories/:historyId',prizeHistoryController.deletePrizeHistoryById);

module.exports = { prizeHistoryRouter };