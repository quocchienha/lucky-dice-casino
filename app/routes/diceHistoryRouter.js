//khai báo thư viện express
const express = require('express');
const { diceHistoryMiddleware } = require('../middlewares/diceHistoryMiddleware');

const diceHistoryController = require('../controllers/diceHistoryController');
//tạo router
const diceHistoryRouter = express.Router();

 
//sủ dụng middle ware
diceHistoryRouter.use(diceHistoryMiddleware);
 
//get all dice
diceHistoryRouter.get('/dice-histories',diceHistoryController.getAllDiceHistory);

//get a dice
diceHistoryRouter.get('/dice-histories/:diceHistoryid', diceHistoryController.getDiceHistoryById);
 
//create a dice
diceHistoryRouter.post('/dice-histories', diceHistoryController.createDiceHistory);

//update a diceHistory
diceHistoryRouter.put('/dice-histories/:diceHistoryid', diceHistoryController.updateDiceHistoryById);

//delete a dice
diceHistoryRouter.delete('/dice-histories/:diceHistoryid', diceHistoryController.deleteDiceHistoryById)

module.exports = { diceHistoryRouter };