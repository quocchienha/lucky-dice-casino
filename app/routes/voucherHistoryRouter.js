//khai báo thư viện express
const express = require('express');
const voucherHistoryMiddleware = require('../middlewares/voucherHistoryMiddleware');
const voucherHistoryController = require('../controllers/voucherHistoryController');
//tạo router
const voucherHistoryRouter = express.Router();

//sủ dụng middle ware
voucherHistoryRouter.use(voucherHistoryMiddleware);

//get all voucher-histories
voucherHistoryRouter.get('/voucher-histories', voucherHistoryController.getAllVoucherHistory);

//get a voucherHistory
voucherHistoryRouter.get('/voucher-histories/:historyId', voucherHistoryController.getVoucherHistoryById);

//create a voucherHistory
voucherHistoryRouter.post('/voucher-histories', voucherHistoryController.createVoucherHistory);

//update a voucherHistory
voucherHistoryRouter.put('/voucher-histories/:historyId',voucherHistoryController.updateVoucherHistoryById);

//delete a voucherHistory
voucherHistoryRouter.delete('/voucher-histories/:historyId',voucherHistoryController.deleteVoucherHistoryById);

module.exports = { voucherHistoryRouter };