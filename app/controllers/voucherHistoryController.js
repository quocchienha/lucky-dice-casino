const VoucherHistory = require('../model/voucherHistory');
//import thư viện mongoose
const mongoose = require('mongoose');

class VoucherHistoryController {
  //
  getAllVoucherHistory(req,res) {
    //B1: thu thập dữ liệu từ req
    // let id = req.query.user
    let {user} = req.query
    let condition = {};
    if(user){
        condition.user = user;
    }
    //B2: validate dữ liệu
    // if(!mongoose.Types.ObjectId.isValid(id)) {
    //   return res.status(400).json({
    //     message: 'UserId is invalid!'
    //   })
    // }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    VoucherHistory.find(condition)
    .populate('user')
    .populate('voucher')
    .exec((error,data)=> {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Get all VoucherHistorys successfully",
        VoucherHistories: data
      })
    })
  }

  createVoucherHistory(req,res){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
      return res.status(400).json({
        message: 'User is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.voucher)) {
      return res.status(400).json({
        message: 'Voucher is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newVoucherHistoryData = {
      _id: mongoose.Types.ObjectId(),
      user: body.user,
      voucher: body.voucher,
    }
    VoucherHistory.create(newVoucherHistoryData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(201).json({
        message: "Create successfully",
        newVoucherHistory: data
      })
    })
  }

  getVoucherHistoryById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'VoucherHistoryId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    VoucherHistory.findById(id)
    .populate('user')
    .populate('voucher')
    .exec((error,data)=> {
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Get VoucherHistory successfully",
        VoucherHistory: data
      })
    })
  }

  updateVoucherHistoryById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'VoucherHistoryId is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
      return res.status(400).json({
        message: 'User is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.voucher)) {
      return res.status(400).json({
        message: 'Voucher is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let voucherHistoryUpdate = {
      user: body.user,
      Voucher: body.Voucher
    }
    if(body.user){
      voucherHistoryUpdate.user = body.user;
    }
    if(body.voucher){
      voucherHistoryUpdate.voucher = body.voucher;
    }
    VoucherHistory.findByIdAndUpdate(id,voucherHistoryUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Update VoucherHistory successfully",
        VoucherHistory: data
      })
    })
  }

  deleteVoucherHistoryById(req,res,next){
  
    let id = req.params.historyId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'VoucherHistoryId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    VoucherHistory.findByIdAndDelete(id,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete VoucherHistory successfully",
      })
    })
    }
} 
module.exports = new VoucherHistoryController;