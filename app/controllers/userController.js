const User = require('../model/user');
//import thư viện mongoose
const mongoose = require('mongoose');
class UserController {
  getAllUser(req,res) {
    //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    User.find()
    .then(users => res.status(200).json(users))
    .catch(error=>{
      res.status(500).json({
        message: error.message
      })
    });
  }

  createUser(req,res){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if(!body.username) {
      return res.status(400).json({
        message: 'username is required'
      })
    }
    if(!body.firstname) {
      return res.status(400).json({
        message: 'firstname is required'
      })
    }
    if(!body.lastname) {
      return res.status(400).json({
        message: 'lastname is required'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newUserData = {
      _id: mongoose.Types.ObjectId(),
      username: body.username,
      firstname: body.firstname,
      lastname: body.lastname,
    }
    User.create(newUserData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(201).json({
        message: "Create successfully",
        newUser: data
      })
    })
  }

  getUserById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.userid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'UserId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    User.findById(id)
      .exec((error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Get user successfully",
        user: data
      })
    })
  }

  updateUserById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.userid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'UserId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.username !== undefined && body.username =="") {
      return res.status(400).json({
        message: 'username is required'
      })
    }
    if(body.firstname !== undefined && body.firstname =="") {
      return res.status(400).json({
        message: 'firstname is required'
      })
    }
    if(body.lastname !== undefined && body.lastname =="") {
      return res.status(400).json({
        message: 'lastname is required'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let userUpdate = {
      username: body.username,
      firstname: body.firstname,
      lastname: body.lastname,
    }
    if(body.username){
      userUpdate.username = body.username;
    }
    if(body.firstname){
      userUpdate.firstname = body.firstname;
    }
    if(body.lastname){
      userUpdate.lastname = body.lastname;
    }
    User.findByIdAndUpdate(id,userUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update user successfully",
        user: data
      })
    })
  }

  deleteUserById(req,res,next){
    
    //B1: thu thập dữ liệu từ req
    let id = req.params.userid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'UserId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    User.findByIdAndDelete(id,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete user successfully",
      })
    })
  }
} 
module.exports = new UserController;